<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="html" encoding="UTF-8" />
    <xsl:template match="/">
        <xsl:text disable-output-escaping="yes">&lt;!DOCTYPE html&gt;</xsl:text>
        <html>
            <head>
                <title>Slider</title>
                <meta name="version" content="2.0.0" />
                <meta name="viewport" content="initial-scale=1, shrink-to-fit=no, viewport-fit=cover, width=device-width, height=device-height" />

                <script src="https://cdnjs.cloudflare.com/ajax/libs/tiny-slider/2.9.2/min/tiny-slider.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tiny-slider/2.9.3/tiny-slider.css"/>
        <style>
            body{
                margin: 0;
                padding: 0;
                border: 0;
                width:100%;
                height:100%;
                background: linear-gradient(rgba(0, 0, 0, 0.6), rgba(2, 2, 2, 0.9));
                overflow:hidden;
            }
            img{
                width:auto;
                height:auto;
                max-width:100%;
                max-height:100vh;
            }
            ul{
                padding-inline-start: 0px;
            }
            li{
                text-align:center;
            }
        </style>
            </head>
            <body>
                <ul class="my-slider">
                    <xsl:for-each select="list/*">
                      <xsl:if test="name(.)='file'">
                        <li>
                            <img>
                                <xsl:attribute name="class">
                                    <xsl:text>tns-lazy-img</xsl:text>    
                                </xsl:attribute>
                                <xsl:attribute name="name">
                                    <xsl:value-of select="name(.)" />
                                    <xsl:text> </xsl:text>
                                    <xsl:value-of select="." />
                                </xsl:attribute>
                                <xsl:attribute name="data-src">
                                    <xsl:value-of select="." />
                                </xsl:attribute>
                             </img>
                        </li>
                      </xsl:if>
                    </xsl:for-each>
                </ul>


                <script><![CDATA[document.addEventListener("DOMContentLoaded", function(event) {
                    var slider = tns({
                        container: '.my-slider',
                        lazyload:true,
                        nav:false,
                        center:true,
                        arrowKeys:true,
                        controls:false,
                        controlsContainer:false,
                        autoplayButtonOutput:false,
                        autoplay:true,
                        items: 1
                    });
                });]]></script>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>
