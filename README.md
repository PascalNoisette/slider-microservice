# Slider microservice 

Web image gallery generated from directory

## Getting started

This will serve the /Pictures/ directory as a web gallery :

```
git clone https://gitlab.com/PascalNoisette/slider-microservice.git
docker run --name some-nginx -p8080:80 -v `pwd`/nginx.conf:/etc/nginx/nginx.conf -v `pwd`/:/conf -v /Pictures/:/usr/share/nginx/html:ro -d nginx:1.19

```

## How is it used

The webpage is displayed on a chromecast with [castDeck](https://nlcamarillo.github.io/castDeck/)

## How does it work

The official [nginx docker image](https://hub.docker.com/_/nginx) embed a [xslt_module](https://nginx.org/en/docs/http/ngx_http_xslt_module.html) that can be enabled in configuration.
I use it to generate a custom directory listing : an appropriate html markup with a [javascript gallery library](https://github.com/ganlanyuan/tiny-slider)
